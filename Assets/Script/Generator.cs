﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class Generator : MonoBehaviour
{
    [SerializeField]
    Button buttonGenerator;
    [SerializeField]
    Button buttonCopy;
    [SerializeField]
    InputField inputFieldQuantity;
    [SerializeField]
    Text databaseContentText;
    [SerializeField]
    Text generatedContentText;
    int quantity = 0;

    [SerializeField]
    Text generatedCountText;
    int countGenerated = 0;

    [SerializeField]
    Text databaseCountText;
    int countDatabase = 0; 

    string[] tempData;

    [SerializeField]
    string domain;

    string fileExtension = ".txt";
    string subFolder = "Database";

    [SerializeField]
    string[] urls;

    List<string> webDataLocation;
    List<string> localDataLocation;

    [SerializeField]
    Dropdown dropdownURL;

    public void UpdateQuantity() {
        int.TryParse(inputFieldQuantity.text,out quantity);
    }

    void createWebDataLocation() {
        webDataLocation = new List<string>();
        foreach (string url in urls) {
            webDataLocation.Add(domain + url + fileExtension);
        }
    }

    void createLocalDataLocation() {
        localDataLocation = new List<string>();
        List<string> ext = new List<string> { fileExtension };
        var files = Directory.GetFiles(Path.Combine(Application.dataPath, subFolder), "*.*", SearchOption.AllDirectories).Where(s => ext.Contains(Path.GetExtension(s)));
        foreach (string url in files) {
            string filename = url.Replace(Path.Combine(Application.dataPath, subFolder), "");
            filename = filename.Replace(@"\", "");
            localDataLocation.Add(filename);
        }
    }

    List<string> createOptionsDropdown() {
        if (RuntimePlatform.WebGLPlayer == Application.platform) {
            createWebDataLocation();
            return new List<string>(webDataLocation);
        } else {
            createLocalDataLocation();
            return new List<string>(localDataLocation);
        }
    }

    void Start()
    {
        Initialize();
    }

    public void Initialize() {
        dropdownURL.ClearOptions();
        List<string> generatedList = createOptionsDropdown();
        dropdownURL.AddOptions(generatedList);
        StartShowDatabase();
        ClearGenerated();
    }

    public void StartShowDatabase() {
        StartCoroutine(ShowDatabase());
    }

    /******************* USER ACTIONS /*******************/
    public void AddGenerated() {
        List<string> data = GetDataCanBeAdded();
        List<int> alreadyUsed = new List<int>();
        int maxSearch = quantity;
        if (quantity > data.Count) maxSearch = data.Count;
        for (int i = 0; i< maxSearch; i++) {
            int random = GetAvailableId(alreadyUsed, data);
            string newText = data[random];
            generatedContentText.text += AddHashtag(newText);
            SetCountGenerated(countGenerated + 1);
        }
        
    }
    List<string> GetDataCanBeAdded() {
        List<string> data = GetDatabaseFromForm();
        List<string> result = new List<string>();
        foreach (string hashtag in data) {
            if(!alreadyAdded(hashtag)) result.Add(hashtag);
        }
        return result;
    }

    public void ClearGenerated() {
        generatedContentText.text = "";
        SetCountGenerated(0);
    }
    List<string> GetDatabaseFromForm() {
        string cleanString = databaseContentText.text.Substring(0, databaseContentText.text.Length - 1); //there is a last " ", it need to be ignored
        return new List<string>(cleanString.Split(" "[0]));
    }
    public void Copy() {
        string result  = generatedContentText.text.Substring(0, generatedContentText.text.Length - 1);
        result = result.Replace(System.Environment.NewLine, "");
        result = result.Replace("\r\n", "").Replace("\r", "").Replace("\n", "");
        Debug.Log(result);
        GUIUtility.systemCopyBuffer = result;
    }
    bool alreadyAdded(string newText) {
        return generatedContentText.text.Contains(newText);
    }
    int GetAvailableId(List<int> alreadyUseds, List<string> data) {
        int max = data.Count;
        int random = Random.Range(0, max);
        foreach (int alreadyUsed in alreadyUseds) {
            if (alreadyUsed == random  || alreadyAdded(data[random])) {
                random = GetAvailableId(alreadyUseds, data);
                break;
            }
        }
        alreadyUseds.Add(random);
        return random;
    }
    /******************* COUNT GENERATED /*******************/
    void SetCountGenerated(int newValue) {
        countGenerated = newValue;
        generatedCountText.text = countGenerated.ToString();
    }
    /******************* COUNT DATABASE /*******************/
    void SetCountDatabase(int newValue) {
        countDatabase = newValue;
        databaseCountText.text = countDatabase.ToString();
    }

    IEnumerator ShowDatabase() {
        yield return UpdateDatabase();
        databaseContentText.text = "";
        foreach (string data in tempData) {
            databaseContentText.text += AddHashtag(data);
        }
        SetCountDatabase(tempData.Length);
    }

    IEnumerator UpdateDatabase() {
        if (RuntimePlatform.WebGLPlayer == Application.platform) {
            yield return UpdateHashtagsByURL(dropdownURL.options[dropdownURL.value].text);
        } else {
            string location = GetAbsoluteDirectory(dropdownURL.options[dropdownURL.value].text);//foreach (string a in generatedList) Debug.Log(a);
            yield return UpdateHashtagsFromTextFile(location);
        }
    }
    string GetAbsoluteDirectory(string filename) {
        return Path.Combine(Application.dataPath, subFolder, filename);
    }
    IEnumerator UpdateHashtagsByURL(string URL) {
        UnityWebRequest www = UnityWebRequest.Get(URL);
        yield return www.SendWebRequest();

        if (www.isNetworkError || www.isHttpError) {
            tempData = new string[] { "No database found" };
        } else {
            tempData = www.downloadHandler.text.Split("\n"[0]);
        }
    }

    IEnumerator UpdateHashtagsFromTextFile(string path) {
        if (File.Exists(path)) {
            StreamReader textFile = new StreamReader(path);
            string fileContents = textFile.ReadToEnd();
            textFile.Close();
            string[] lines = fileContents.Split("\n"[0]);
            tempData = lines;
        } else tempData = new string[] {"No database found"};
        yield return null;
    }

    string AddHashtag(string added) {
        return added + " ";
    }
}
