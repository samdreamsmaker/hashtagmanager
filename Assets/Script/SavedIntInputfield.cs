﻿using UnityEngine;
using UnityEngine.UI;

public class SavedIntInputfield : MonoBehaviour
{
    [SerializeField]
    InputField inputField;

    [SerializeField]
    string variableName;

    [SerializeField]
    int defaultValue;

    void Start()
    {
        inputField.text = GetValue().ToString();
    }

    int GetValue() {
        return PlayerPrefs.GetInt(variableName, defaultValue);
    }

    public void SetValue() {
        PlayerPrefs.SetInt(variableName, int.Parse(inputField.text));
    }
}
